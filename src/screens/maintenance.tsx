import React from 'react';
import {Text, View} from 'react-native';

const Maintenance = () => {
  return (
    <View>
      <Text>This is Maintentance Mode</Text>
    </View>
  );
};

export default Maintenance;
